<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Cryptommer\Smsir\Smsir;

use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // save in file

        $data=DB::table('users')->pluck('name','email');
        $path=fopen(storage_path('app/Register_User.doc'),"w+") or die("Error");
        $count=0;

        foreach ($data as $name => $email) {
            $count++;
            fwrite($path , $count."-".$email.":".$name."\n \n");

        }

        fclose($path);

        // send message

        $carbon = new Carbon('-30 seconds');
        if (DB::table('users')->latest()->pluck('created_at')->first()>=$carbon)
        {
            $number = DB::table('users')->latest()->pluck('number')->first();
            $sendname = DB::table('users')->latest()->pluck('name')->first();
            $message ="Dear $sendname ..!"."\n\n"."Your Registeration in FIAT was successful"."\n\n"."Thaks for choice us ;)"."\n\n" .'https://fiat-car.iran.liara.run';

            $send = smsir::Send();
            $send->bulk($message, [str($number)], null, "30007732006288");


            return view('home');
        }
        else
        {
            return view('home');
        }


    }
}
