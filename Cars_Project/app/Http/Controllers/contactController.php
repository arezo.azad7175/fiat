<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class contactController extends Controller
{
    public function contact()
    {
        return view('contact-us');
    }

    public function contact_post(Request $data)
    {
        $validation = $data->validate([
            'fullname'=>'required',
            'email'=>'required | email',
            'subject'=>'required | max : 60',
        ],
        [
            'required'=>'please fill :attribute field',
            'email'=>'please Enter the correct email',
            'max'=>'message is to long',
        ]);


        if($validation)
        {
            DB::table('contact_')->insert([
                'fullname'=>$data->fullname ,
                'email'=>$data->email,
                'subject'=>$data->subject,
                'created_at'=>now(),
                'updated_at'=>now(),

            ]);

            return redirect()->back()->with('success','we resived your message successfully');
        }
        else
        {
            return redirect()->back()->withErrors();
        }






    }
}
