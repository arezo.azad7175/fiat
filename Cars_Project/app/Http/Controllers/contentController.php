<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Session;


class contentController extends Controller
{
    public function main () {
        return view('content');
    }



    public function captcha_validate(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        if ($validator->fails()) {
           return redirect()->Back()->withInput()->withErrors($validator);

        }else{
           Session::flash('message','Form submit Successfully.');
        }
        return redirect('/');
     }
}
