
// EXTERIOR

function gold_Car()
{
  document.getElementById('img-btn').style.left = '46px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/gliratti-gold.jpg";
  document.getElementById('detail-value').innerText = "Bronzo Tan* (metallic)";
}

function black_Car()
{
  document.getElementById('img-btn').style.left = '93px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/hip-hop-black_punto.jpg";
  document.getElementById('detail-value').innerText = "Hip Hop Black* (metallic)";
}

function minimal_grey_Car()
{
  document.getElementById('img-btn').style.left = '136px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/minimal-grey_punto.jpg";
  document.getElementById('detail-value').innerText = "Minimal Grey* (metallic)";
}

function white_Car()
{
  document.getElementById('img-btn').style.left = '179px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/pearl-white.jpg";
  document.getElementById('detail-value').innerText = "Bossanova White* (metallic)";
}
function Red_Car()
{
  document.getElementById('img-btn').style.left = '226px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/Exotica-Red.jpg";
  document.getElementById('detail-value').innerText = "Exotica Red* (metallic)";
}
function Magnesia_grey_Car()
{
  document.getElementById('img-btn').style.left = '272px';
  document.getElementById('img-btn').style.visibility = "visible";
  document.getElementById('image-ext').src = "images/exterior/Magnesia-grey.jpg";
  document.getElementById('detail-value').innerText = "Magnesio Grey* (metallic)";
}

// LIGHTS-slide

function btn_slide_1()
{
  document.getElementById('btn-img-1').src = "images/light-slides/reindeer_headlamp.png";
  
  document.getElementById('btn-img-2').src = "images/light-slides/chrome_deta.png";
  document.getElementById('btn-img-3').src = "images/light-slides/follow_me_headlamps.png";
}

function btn_slide_2()
{
  document.getElementById('btn-img-2').src = "images/light-slides/chrome_detailing.png";

  document.getElementById('btn-img-1').src = "images/light-slides/reindeer_hea.png";
  document.getElementById('btn-img-3').src = "images/light-slides/follow_me_headlamps.png";
}

function btn_slide_3()
{
  document.getElementById('btn-img-3').src = "images/light-slides/follow_me_headlamps - Copy.png";

  document.getElementById('btn-img-1').src = "images/light-slides/reindeer_hea.png";
  document.getElementById('btn-img-2').src = "images/light-slides/chrome_deta.png";
}

