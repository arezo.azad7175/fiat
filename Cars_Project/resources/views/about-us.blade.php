@extends('layouts.master')
@section('content')

 <!--main picture  -->
 <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

        <div class="about">
           <img src="{{asset('images/about-us/FiatID_history.jpg')}}" alt="">
              <div>
                  <h1>DISCOVER THE<br>FIAT EXPERIENCE</h1>
                <p><strong>Navigate through innovation<br> designed for the fun, practical and <br>stylish</strong></p>
           </div>
       </div>
   </div>
 </div>


 <!-- title HISTORY -->
 <div class="row history-title">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div id="title-1">
          <div id = "title-2" >
              <div id= "title-3">
                   <div id = "title-4">
                       <h2>HISTORY</h2>
                  </div>
              </div>
          </div>
      </div>
    </div>
 </div>


 <!-- slide-1 -->

 <div class="row row-slide">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-slide">
        <div id="carouselExampleFade" class="carousel slide carousel-fade slide-1" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('images/about-us/slide-1/slide-1-1.png')}}" class="d-block w-100" alt="...">
                </div>
               <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-1/slide-1-2.png')}}" class="d-block w-100" alt="...">
               </div>
               <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-1/slide-1-3.png')}}" class="d-block w-100" alt="...">
               </div>
               <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-1/slide-1-4.png')}}" class="d-block w-100" alt="...">
               </div>
               <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-1/slide-1-5.png')}}" class="d-block w-100" alt="...">
               </div>
               <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-1/slide-1-6.png')}}" class="d-block w-100" alt="...">
               </div>
           </div>
           <button class="carousel-control-prev " type="button" data-bs-target="#carouselExampleFade" data-bs-slide="prev">
              <span class="carousel-control-prev-icon btn-prev-slide-1" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
           </button>
           <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleFade" data-bs-slide="next">
              <span class="carousel-control-next-icon btn-next-slide-1" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
           </button>
       </div>
    </div>
 </div>

  <!-- title HISTORY -->
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div id="title-1">
          <div id = "title-2" >
              <div id= "title-3">
                   <div id = "title-4">
                       <h2>ICONIC CARS</h2>
                  </div>
              </div>
          </div>
      </div>
    </div>
 </div>


 <!-- slide-2 -->

 <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-slide">
        <div id="carouselExampleFade" class="carousel slide carousel-fade slide-2" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{asset('images/about-us/slide-2/slide-2-1.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-2.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-3.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-4.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-5.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-6.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-7.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-8.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-9.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-10.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-11.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-12.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-13.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-14.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-15.png')}}" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="{{asset('images/about-us/slide-2/slide-2-16.png')}}" class="d-block w-100" alt="...">
                </div>
             </div>
        </div>
    </div>
 </div>



@endsection
