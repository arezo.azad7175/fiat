@extends('layouts.master')
@section('content')


 <div class="row contact-main">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 col-main" >
        <img src="{{asset('images/contact-us/service_stations.jpg')}}" alt="">

        <div class="container filds border border-secondary">


            {{-- flash sessions --}}

            @if(Session::has('success'))
                <div id = "success" role="alert" aria-live="assertive" aria-atomic="true">
                    <div class="toast-header">

                        <img src="{{asset('images/content&master/logo.png')}}" class="rounded me-2" alt="...">
                        <strong class="me-auto">Message</strong>
                        <a href="{{route('contact-us')}}">
                            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                        </a>

                    </div>

                   <div class="toast-body">
                       <p>{{Session::get('success')}}</p>
                   </div>

               </div>

            @elseif($errors->any())

            <div id = "error" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header">

                    <img src="{{asset('images/content&master/logo.png')}}" class="rounded me-2" alt="...">
                    <strong class="me-auto">Message</strong>
                    <a href="{{route('contact-us')}}">
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </a>

                </div>

               <div class="toast-body">
                @foreach ($errors->all() as $error )
                    <p>{{$error}}</p>
                @endforeach

               </div>

           </div>

            @endif


            <!-- form-contact -->


            <form method="post" action="{{route('contact-us-post')}}">

               @csrf
               <label for="fullname">Full Name:</label>
               <input type="text" id="fullname" placeholder="Your Full Name" name="fullname" class="form-control">
               <br>
               <label for="email">Email Address:</label>
               <input type="text" id="email"  placeholder="Your Email Address" name="email"class="form-control ">
               <br>
               <label for="subject">Message:</label>
               <textarea id="subject"placeholder="Write Your Message . . ."  name="subject" style="height:200px" class="form-control"></textarea>
               <br>
               <input type="submit" value="Submit" class="btn btn-secondary btn-contact">
           </form>
       </div>

    </div>
 </div>
@endsection
