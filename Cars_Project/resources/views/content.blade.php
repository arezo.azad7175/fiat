@extends('layouts.master')
@section('content')

 <!-- first picture -->

    <div class="row" id = "main">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <img src="{{asset('images/content&master/main.jpg')}}" alt="">
            <h5>FIAT</h5>
            <h1>PUNTO EVO</h1>
            <p>Life just Become more intersting</p>

            <div id= "box-main">
            </div>
            <div class = "text-main">
              <p id="txt-box-1">Fuel efficiency+:</br>20.0 km/l</br>Diesel</p>
              <p id="txt-box-2">15.8 km/l</br>Petrol</p>
            </div>


        </div>
    </div>


 <!-- second part -->
 <!-- title -->
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
       <div id="title-1">
           <div id = "title-2" >
               <div id= "title-3">
                   <div id = "title-4">
                       <h2>SPECIAL OFFERS</h2>
                   </div>
              </div>
          </div>
      </div>
  </div>
</div>


 <!-- content offer -->
   <div class="row" >
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12" id="offer">
            <p>Current FIAT Punto Evo offers:</p>
            <img src="{{asset('images/content&master/puntoevo.jpg')}}" alt="">
        </div>
    </div>

 <!-- title model -->
<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

      <div id="title-1">
          <div id = "title-2" >
              <div id= "title-3">
                   <div id = "title-4">
                       <h2>MODELS</h2>
                  </div>
              </div>
          </div>
      </div>
   </div>
</div>


 <!-- content model -->
 <div class="row" id = "model">

    <div class="col-sm-6 col-md-6 col-lg-6  col-xl-6 model-1">
        <img src="{{asset('images/content&master/magnesia_grey.png')}}" alt="">
        <br>
        <div>
            <p class="model-text-1">FIAT</p>
            <h3 class="model-text-2">PUNTO EVO</h3>
            <p class="model-text-3">Active</p>
        </div>

    </div>

    <div class="col-sm-2 col-md-2 col-lg-2  col-xl-2 model-2">
        <img src="{{asset('images/content&master/white.png')}}" alt="">

        <div>
            <p class="model-text-1">FIAT</p>
            <h3 class="model-text-2">PUNTO EVO</h3>
            <p class="model-text-3">Dynamic</p>

            <p class="model-text-4">1.2L FIRE Petrol</p>
            <p class="model-text-4" >1.3L MULTIJET® <br> 93 ps Diesel</p>

        </div>

    </div>

    <div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 model-3">

        <img src="{{asset('images/content&master/gold.png')}}" alt="">

        <div>
            <p class="model-text-1">FIAT</p>
            <h3 class="model-text-2">PUNTO EVO</h3>
            <p class="model-text-3">Emotion</p>

            <p class="model-text-4" >1.3L MULTIJET® <br> 93 ps Diesel</p>
        </div>
    </div>

 </div>

 <!-- title GALLERY -->
 <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 ">

      <div id="title-1">
          <div id = "title-2" >
              <div id= "title-3">
                  <div id = "title-4">
                      <h2>GALLERY</h2>
                  </div>
              </div>
          </div>
       </div>
    </div>
 </div>


 <!-- content GALLERY -->
 <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">

       <div id ="gallery-text" >
            <span >Just like life </br>It keeps getting interesting</span>
            <h3>Splendid, strong and futuristic</h3>
            <br>
            <p>
                No journey is constant. Your ride should evolve too. Designed by the world's leading Italian designers, the new Punto Evo is a vivid blend of sexy and sporty. Thoughtful Follow Me lamps, superior ride handling and safety, revolutionary
                Blue &amp; Me connectivity, a stylish new exterior and swanky interiors make the car a much anticipated milestone in a grand legacy. Add best in class ground clearance coupled with the biggest wheel and widest tyres, and you've
                got yourself a great drive that just became a lot more interesting.
            </p>
        </div>

        <div id="gallery-image">
            <img src="{{asset('images/content&master/gallery_bg.jpg')}}" alt="" >
        </div>

        <!-- slide-gallery -->

        <div id="carouselExampleIndicators" class="carousel slide">

          <div class="carousel-indicators" >
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active  btn" aria-current="true" aria-label="Slide 1"style="background-color: #8a1c2d;);"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" class= "btn" aria-label="Slide 2 " style="background-color: #8a1c2d;"></button>
          </div>

            <div class="carousel-inner">
                <div class="carousel-item active" >
                    <div class="row">

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/icontext_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/digitalsystem_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/seating_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/mirrors_thumb.jpg')}}" >
                           </div>

                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/rearlamp.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/foglamp.jpg')}}" >
                            </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/frontgrill_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/steriosystem_thumb.jpg')}}" >
                           </div>

                        </div>


                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/beachroad_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/chargingunit_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/stearing_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/cupholder1_thumb.jpg')}}" >
                            </div>

                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/seatbelt_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/leatherseating_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/rearhood_thumb.jpg')}}" >
                            </div>

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-first/sportivo-seats-thumb.jpg')}}" >
                            </div>

                        </div>

                    </div>
               </div>
              <div class="carousel-item">
                   <div class="row">

                       <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                          <div class="slide-image">
                               <img src="{{asset('images/slides-gallery/slide-second/wheel_thumb.jpg')}}" >
                           </div>

                          <div class="slide-image">
                               <img src="{{asset('images/slides-gallery/slide-second/Puntoevo-rear-view_thumb.jpg')}}" >
                          </div>

                          <div class="slide-image">
                               <img src="{{asset('images/slides-gallery/slide-second/reverse-sensors-thumb.jpg')}}" >
                          </div>

                          <div class="slide-image">
                               <img src="{{asset('images/slides-gallery/slide-second/sideview1_thumb.jpg')}}" >
                          </div>
                      </div>

                      <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/chargingunit_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/seating_black_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/airbags_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/wheel_thumb.jpg')}}" >
                           </div>

                        </div>


                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">

                            <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/cupholder_thumb.jpg')}}" >
                           </div>

                           <div class="slide-image">
                               <img src="{{asset('images/slides-gallery/slide-second/headlamp_thumb.jpg')}}" >
                          </div>

                          <div class="slide-image">
                                <img src="{{asset('images/slides-gallery/slide-second/interiorseatingview_thumb.jpg')}}" >
                          </div>

                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <div class="slide-image">
                            <img src="{{asset('images/slides-gallery/slide-second/puntoevo_thumb.jpg')}}" >
                       </div>

                        <div class="slide-image">
                            <img src="{{asset('images/slides-gallery/slide-second/mirror.jpg')}}" >
                       </div>

                      <div class="slide-image">
                            <img src="{{asset('images/slides-gallery/slide-second/wheel2_thumb.jpg')}}" >
                      </div>

                    </div>
              </div>

            </div>

            <button class="carousel-control-prev btn-slide-1" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next btn-slide-2" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
          </button>
      </div>




    </div>
 </div>


 <!-- title EXTERIOR -->
 <div class="row title-exterior">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">

       <div id="title-1">
           <div id = "title-2" >
               <div id= "title-3">
                   <div id = "title-4">
                       <h2>EXTERIOR</h2>
                  </div>
              </div>
          </div>
       </div>
   </div>
 </div>

 <!-- content EXTERIOR part 1-->
 <div class="row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">
        <div id="exterior-image">

            <img src="{{asset('images/exterior/gliratti-gold.jpg')}}" alt="" id="image-ext">


        </div>
        <div id="exterior-text">
            <h2>Royal on road</h2>
            <h3>Creating an impression, every day.</h3>
            <p>Embodying the fine Italian design, a majestic outside, and elegant interior, the new Punto Evo comes in breathtaking hues, each one being better than the other. Choose from Bronzo Tan, Red, White, Black, Minimal Grey or fresh shades like Magnesio Gray.</p>
            <ul>
                <li id="btn-1" >
                    <div >
                        <img src="{{asset('images/exterior/rollover_bg.png')}}" alt=""  id="img-btn" >
                        <img src="{{asset('images/exterior/tuscan_wine.png')}}" alt="" title = "" onclick = "gold_Car()">
                    </div>
                </li>

                <li id="btn-2">
                    <div >
                        <img src="{{asset('images/exterior/hip_hop_black.png')}}" alt="" onclick = "black_Car()">
                    </div>
                </li>

                <li id="btn-3">
                    <div>
                        <img src="{{asset('images/exterior/minimal_grey.png')}}" alt="" onclick = "minimal_grey_Car()">
                    </div>
                </li>

                <li id="btn-4">
                    <div>
                        <img src="{{asset('images/exterior/bossa_nova_white.png')}}" alt="" onclick = "white_Car()">
                    </div>
                </li>

                <li id="btn-5">
                    <div>
                        <img src="{{asset('images/exterior/exotica_red.png')}}" alt="" onclick = "Red_Car()">
                    </div>
                </li>

                <li id="btn-6">
                    <div>
                        <img src="{{asset('images/exterior/magnesio_grey.png')}}" alt="" onclick = "Magnesia_grey_Car()">
                    </div>
                </li>
            </ul>

            <div class="selected-details">
                <span id="detail-text">Color shown: <span id="detail-value"> Bronzo Tan* (metallic)</span></span>
                <br>
                <span id ="selected-text">*Additional charges applicable for metallic colors.</span>
            </div>

        </div>
    </div>
 </div>

 <!-- content EXTERIOR part 2-->

 <div class="row EXTERIOR-part-2-row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 EXTERIOR-part-2">

        <img src="{{asset('images/exterior/alloy_wheel.jpg')}}" alt="">
        <div>
            <h4>Bigger, wider, and a <br> lot stylish</h4>
            <span>38.1 cm (15) alloy wheels for a</br> smooth ride on any road</span>
            <p>The sturdy alloy wheels are the first-of-its-kind in the</br> hatchback segment, ensuring you a smooth drive in all</br> weathers, on all roads.</p>
            <h6>Wheel shown: 38.1 cm (15) Aluminum alloy wheels</h6>
        </div>


    </div>
 </div>


 <!-- light-slide  -->

 <div class="row lights-row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 lights">

        <div id="carouselExampleCaptions" class="carousel slide lights-slide" data-bs-ride="false">

           <span>Select an exterior feature</span>
           <div class="carousel-indicators btn-slide">
                <hr id="line-1">
                <img src="{{asset('images/light-slides/reindeer_hea.png')}}" alt="" class = "btn-img"  id ="btn-img-1">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1" onclick = "btn_slide_1()"></button>

                <img src="{{asset('images/light-slides/chrome_deta.png')}}" alt="" class = "btn-img"  id ="btn-img-2">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2" onclick = "btn_slide_2()"></button>

                <img src="{{asset('images/light-slides/follow_me_headlamps.png')}}" alt="" class = "btn-img"  id ="btn-img-3">
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3" onclick = "btn_slide_3()"></button>
                <hr id="line-2">
            </div>

          <div class="carousel-inner">

          <!-- slide-1 -->
               <div class="carousel-item active " >
                   <img src="{{asset('images/light-slides/light_your_way.jpg')}}" class="d-block w-100 img-slides-1" alt="...">
                   <div class="carousel-caption d-none d-md-block  text-slide-1">
                        <h3>Light your way</h3>
                        <p>
                        A better and clearer visibility with Reindeer Headlamps
                        <br>We have our eyes on what lies ahead. The Reindeer Headlamps provide
                        <br>better visibility and style, and earn fresh envy.
                       </p>
                   </div>
              </div>

            <!-- slide-2 -->
              <div class="carousel-item ">
                   <img src="{{asset('images/light-slides/intricate_detailing.jpg')}}" class="d-block w-100 img-slides-2" alt="...">
                   <div class="carousel-caption d-none d-md-block text-slide-2">
                       <h3>Intricate detailing, streamlined<br> experience</h3>
                       <p>
                        Stand out in the crowd with fine artistic finish
                        <br>From an all new grille to a new number plate, the artistic finish and elaborative
                        <br>chrome detailing of new Punto Evo accentuates its sturdy frame.
                      </p>
                  </div>
              </div>

            <!-- slide-3 -->
              <div class="carousel-item ">
                    <img src="{{asset('images/light-slides/accompanying_you_journey_end.jpg')}}" class="d-block w-100 img-slides-3" alt="...">
                    <div class="carousel-caption d-none d-md-block text-slide-3 ">
                       <h3>Accompanying you<br> till journey's end</h3>
                       <p>
                        The new Punto Evo accompanies you till your journey's end.<br> Its programmable Follow Me technology lets you time your<br> headlights depending on the distance you need to travel, so<br> you can walk home safely even when you park in the<br>
                        remotest corner.
                      </p>
                  </div>
              </div>
          </div>
          <button class="carousel-control-prev  btn-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
            <span class="carousel-control-prev-icon btn-prev-txt" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next btn-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
            <span class="carousel-control-next-icon btn-next-txt" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
      </div>
   </div>
 </div>

  <!-- title INTERIOR -->
  <div class="row  title-lights">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">

       <div id="title-1">
           <div id = "title-2" >
               <div id= "title-3">
                   <div id = "title-4">
                       <h2>INTERIOR</h2>
                  </div>
              </div>
          </div>
       </div>
   </div>
 </div>

 <!-- conyent INTERIOR -->
 <!-- part-1 -->
 <div class="row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 interior-1">
        <img src="{{asset('images/interior/control_with_style.jpg')}}" alt="">

        <div>
            <h3>Control with style</h3>
            <span>Your space, your comfort</span>
            <p>The new Punto Evo's sexy curvilinear new dashboard <br>features dual tone soft touch, ambient lighting, sleek push<br> buttons, smarter controls, and a glove compartment with<br> enough room to accommodate everything of importance<br> to you. </p>
        </div>
    </div>
 </div>

 <!-- part-2 -->
 <div class="row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 interior-2">
        <img src="{{asset('images/interior/enjoy_non_fatigue_journey.jpg')}}" alt="">
        <div>
           <h3>Enjoy a non-<br>fatigue<br> journey</h3>
           <span>Legroom for all</span>
           <p>No need to dread long journeys anymore. The<br> new Punto Evo provides ample legroom so you<br> can conveniently stretch your limbs on the go.</p>
        </div>
    </div>
 </div>

 <!-- title SAFETY -->
 <div class="row title-safety">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">

       <div id="title-1">
           <div id = "title-2" >
               <div id= "title-3">
                   <div id = "title-4">
                       <h2>SAFETY</h2>
                  </div>
              </div>
          </div>
       </div>
   </div>
 </div>

 <!-- content SAFETY -->

 <div class="row ">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 safety">
        <img src="{{asset('images/safety/built_like_a_tank.jpg')}}" alt="">
        <div>
           <h3>Built like a tank</h3>
           <span>High-tensile steel body immune to <br> shocks</span>
           <p>The new Punto Evo is built like a tank. Its sturdy frame minimizes<br> primary damage, while the indoor mechanism reduces impact.<br> Insurance covers damages; our assurance covers the fear of the<br> unknown.</p>
        </div>
    </div>
 </div>


 <!-- title ENGINE -->
 <div class="row title-engine">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">

       <div id="title-1">
           <div id = "title-2" >
               <div id= "title-3">
                   <div id = "title-4">
                       <h2>ENGINE</h2>
                  </div>
              </div>
          </div>
       </div>
   </div>
 </div>
<!-- content engine -->
<!-- part-1 -->
<div class="row">
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 engine-text-p1">
        <h3>High<br> performance,no <br>maintenance</h3>
        <span>Dynamic, strong, and eco-friendly core</span>
        <p>The new Punto Evo gets all the basics right. At the<br> heart of the car is an acclaimed and trusted engine <br> which is fuel-efficient and dynamic, yet eco-friendly,<br> making it a high performance package at next to<br> nothing maintenance.</p>

        <div class="engin-text-box-1">
            <img src="{{asset('images/engine/Multijet-Logo.png')}}" alt=""><br>
            <span>Multi-Jet (Diesel)</span>
            <p>The new Punto Evo's award winning* compact diesel engine<br> powers millions of cars worldwide. It offers improved<br> performance, better fuel efficiency, a smoother drive, and has <br>set the benchmark for diesel engines in India.<br>*International engine of the year in 2005.</p>
        </div>

        <div class="engin-text-box-2">
            <img src="{{asset('images/engine/Fire.png')}}" alt=""><br>
            <span>Fully integrated robotized engine(Petrol)</span>
            <p>The petrol version of the new Punto Evo features F.I.R.E., a<br> revolutionary advancement in FIAT engine technology. Delivering<br> stellar performance with welcome fuel efficiency, F.I.R.E. makes it<br> just perfect for your daily commute.</p>
        </div>

    </div>

    <!-- image -->
    <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 engine-img-p1">
        <img src="{{asset('images/engine/high_performance_engine.png')}}" alt="">
    </div>
</div>

<!-- part-2 -->

<div class="row">
    <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12 engin-image-p2">
        <img src="{{asset('images/engine/smile_longer_stop_less.jpg')}}" alt="">

        <div>
            <h3>Smile longer,<br> stop less</h3>
            <span>Efficiency found its fun side</span>
            <p>Drive past fuel pump after fuel pump as your vehicle <br>has an estimated 20.0 km/l mileage</p>

            <div class="text-box-p2">

                <h4>Diesel</h4>

                <p>1.3L MULTIJET 93ps Active/ Dynamic/</p>

                <p>Emotion models</p>

                <h5>20.0 <span> km/l<sup>+</sup></span></h5>

                <h4>Petrol</h4>

                <p>1.2L FIRE Dynamic model</p>

                <h5> 15.8 <span> km/l<sup>+</sup></span></h5>

            </div>
        </div>
    </div>
</div>



@endsection



