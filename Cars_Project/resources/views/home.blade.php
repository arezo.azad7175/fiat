@extends('layouts.master')

@section('content')
<div class="container-fluid">

 <!-- content -->

 <div class="row ">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 content">
            <img src="{{asset('images/auth/Abarth_Punto_sporty_skirting.jpg')}}" alt="">
            <div>
                <h3>{{ __('WELLCOME To FIAT !') }}</h3>
                <h4>{{ Auth::user()->name }} </h4>
                <p>You Are Login</p>

                <a href="{{route('logout')}}" class="log-out links">
                    <label for="icofont-logout">LOGOUT</label>
                    <i class="icofont-logout"></i>
                </a>

            </div>

        </div>
   </div>

   <!-- title content -->
   <div class="row title-content">
       <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">
          <div id="title-1">
               <div id = "title-2" >
                   <div id= "title-3">
                       <div id = "title-4">
                          <h2>CONTENT</h2>
                       </div>
                  </div>
              </div>
           </div>
       </div>
   </div>

    <!-- main -->
    <div class="row main">
        <div class="col-sm-12  col-md-12 col-lg-12 col-xl-12">
            <img src="{{'images/auth/Abarth_Punto_banner.jpg'}}" alt="">
            <h5>FIAT</h5>
            <h2>ABARTH PUNTO</h2>
            <p>The steering wheel isn't just for steering. It's for holding on.</p>

            <div id= "box-main">
            </div>
            <div id = "text-main">
                <p id="txt-box-1">145 bhp</p>
                <p id="txt-box-2">0-100 in 8.8 Secs</p>
            </div>
        </div>
    </div>




    <div class="row justify-content-center">
        <div class="col-sm-8 col-md-8 col-lg-8 col-xl-8 home-content">
            <div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>


        </div>
    </div>






            </div>
        </div>
    </div>
</div>
@endsection
