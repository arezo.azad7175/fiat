<html lang="en">

<head>

    <!--title-->
    <title>Buy Fiat Avventura</title>
    <link rel="icon" href="{{asset('images/content&master/logo_title.png')}}">


    <!-- bootstrap-links -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>



    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- css & java script links -->

    @vite(['resources/css/style-master.css' , 'resources/css/style-content.css' , 'resources/css/auth.css', 'resources/css/about-us.css' , 'resources/css/contact-us.css'])

    <script src="{{asset('js/app.js')}}"></script>


    <!-- icofont -->

    <link rel="stylesheet" href="{{asset('icofont/icofont.min.css')}}">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- captcha --}}

    <!-- Include script -->
   <script type="text/javascript">
    function callbackThen(response) {

      // read Promise object
      response.json().then(function(data) {
        console.log(data);
        if(data.success && data.score >= 0.6) {
           console.log('valid recaptcha');
        } else {
           document.getElementById('contactForm').addEventListener('submit', function(event) {
              event.preventDefault();
              alert('recaptcha error');
           });
        }
      });
    }

    function callbackCatch(error){
       console.error('Error:', error)
    }
    </script>

    {!! htmlScriptTagJsApi([
       'callback_then' => 'callbackThen',
       'callback_catch' => 'callbackCatch',
    ]) !!}

</head>


<body>
    <div class="container_fluid" >


    <!-- header -->

        <div class="row" id="header">

            <div class="col-12">

                <img src="{{asset('images/content&master/logo.png')}}" alt="" id="logo-1">
                <img src="{{asset('images/content&master/puntoevo_logo.jpg')}}" alt="" id = "logo-2">

               <a href="{{route('main')}}" class="home links">
                   <i class="icofont-home"></i>
               </a>

                <div id ="signup">
                  <a href="{{route('register')}}" class="links">
                      <p>Sign Up</p>
                  </a>
                </div>


                <div id ="login">
                   <a href="{{route('login')}}" class="links">
                       <p>Login</p>
                    </a>
                </div>


                <div id="about">
                    <a href="{{route('about-us')}}" class="links">
                       <p>About FIAT</p>
                    </a>
                </div>

                <div id = "contact">
                   <a href="{{route('contact-us')}}" class="links">
                       <P>Contact Us</P>
                    </a>
                </div>


                <div  id = "text">
                    <P>Toll Free </br>Number</br>1800 209 5556</P>
                </div>



            </div>


        </div>

        <!-- content -->

        @yield('content')


        <!-- footer -->

        <div class="row" id = "footer" >


            <div class="col-1">

                <div id = "box">

                    <div id = "border-box">

                        <p id="box-txt-1">GO SOCIAL</br>WITH</br>FIATINDIA</p>
                        <img src="{{asset('images/content&master/logo.png')}}" alt="" id="logo-box">
                        <b id="box-txt-2">NATION</b>
                        <p id="box-txt-3">THE ONLINE HUB FOR ALL </br>FIAT INDIA AMAZINGNESS</p>

                        <img src="{{asset('images/content&master/instagram.png')}}" alt="" class = "social-icons">
                        <img src="{{asset('images/content&master/twitter.png')}}" alt="" class = "social-icons">
                        <img src="{{asset('images/content&master/youtube.png')}}" alt="" class = "social-icons">
                        <img src="{{asset('images/content&master/facebook.png')}}" alt="" class = "social-icons">

                    </div>

                </div>
            </div>



            <div class="col-11" id = "footer-part-2">

                <div id="footer-txt-1">
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam, omnis alias accusantium, suscipit dolores sapiente vero amet ex commodi et qui sequi quis at! Fuga cupiditate magni praesentium natus delectus.</br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi quidem magni aliquid veritatis voluptate, magnam sint, consequatur facere in dolor dolores porro repudiandae aperiam expedita quae omnis libero tempora? Vitae?</p>
                </div>

                <div id = "social-media">
                    <p>Follow Us</p>
                    <img src="{{asset('images/content&master/social-media.png')}}" alt="" >
                </div>

                <div id = "footer-txt-2">
                    <p>Privacy Policy | International | Site Map | News | Contact Us | Careers | Legal, Safety and Trademark Information</p>
                </div>

                <div id="footer-txt-3">

                    <img src="{{asset('images/content&master/footer.png')}}" alt="">
                    <p>
                        © FCA India Automobiles Private Limited f.k.a Fiat Group Automobiles India Private Limited .</br>
                        FCA India Automobiles Private Limited f.k.a Fiat Group Automobiles India Private Limited reserves the right to change without notice the colours, equipment</br>
                        specifications and models. Accessories shown in the pictures and features mentioned may not be a part of the standard equipment and could differ with</br>
                        variants. Actual colour of vehicle and upholstery might differ.</br>
                        *Ex-showroom price excludes road taxes, registration fees & insurance. Starting at price refers to the base model, optional equipment not included.</br>
                        A more expensive model may be shown. Pricing and offers may change at any time without notification. To get full pricing details, contact your nearest </br>
                        dealer.</br>
                       *Offers applicable only on selected cities and selected models.
                    </p>
                </div>



            </div>

        </div>

    </div>


</body>

</html>
