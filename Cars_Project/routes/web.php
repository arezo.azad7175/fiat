<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\aboutController;
use App\Http\Controllers\contactController;
use App\Http\Controllers\logoutController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\contentController::class , 'main'])->name('main');
Route::post('/page-captcha', [App\Http\Controllers\contentController::class , 'captcha_validate'])->name('captcha_validate');

Auth::routes();

Route::get('/logout', [App\Http\Controllers\logoutController::class, 'logout'])->name('logout');

Route::post('/contact-us', [App\Http\Controllers\contactController::class, 'contact_post'])->name('contact-us-post');
Route::get('/contact-us', [App\Http\Controllers\contactController::class, 'contact'])->name('contact-us');

Route::get('/about-us', [App\Http\Controllers\aboutController::class, 'about'])->name('about-us');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');










