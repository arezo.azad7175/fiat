import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/js/app.js',
                'resources/sass/app.scss',
                'resources/sass/_variables.scss',
                'resources/css/style-content.css',
                'resources/css/contact-us.css',
                'resources/css/auth.css',
                'resources/css/about-us.css',
                'resources/css/style-master.css',

            ],
            refresh: true,
        }),
    ],
});
